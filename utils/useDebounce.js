import { useEffect, useState } from 'react';

export const useDebounce = (value, milliseconds) => {
  const [debouncedValue, setDebouncedValue] = useState('');

    useEffect(() => {
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, milliseconds);

      return () => {
        clearTimeout(handler)
      }
    }, [value, milliseconds]);

    return debouncedValue;
}