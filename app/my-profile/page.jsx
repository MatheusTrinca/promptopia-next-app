'use client'; 

import React, { useEffect, useState } from 'react'
import Profile from '@components/Profile';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';

const MyProfile = () => {

  const [posts, setPosts] = useState([])
  const { data: session } = useSession();
  const router = useRouter();

  useEffect(() => {
    const fetchPosts = async () => {
      const response = await fetch(`/api/users/${session?.user.id}/posts`);

      const data = await response.json();

      setPosts(data)
    };
  
    session?.user.id && fetchPosts();
  }, [session?.user.id])
  
  
  const handleEdit = (post) => {
    router.push(`/update-prompt?id=${post._id}`)
  };  

  const handleDelete = async (post) => {
    const isConfirmed = confirm('Are you sure want to delete this prompt?');

    if(!isConfirmed) return;

    try {
      await fetch(`/api/prompt/${post._id}`, {
        method: 'DELETE'
      })

      const filteredPosts = posts.filter(oldPost => oldPost._id !== post._id);

      setPosts(filteredPosts);
            
    } catch (error) {
      console.log(error)
    }

  };
  
  return (
    <Profile
      name="My"
      desc="Welcome to your personalized profile page. Share your exceptional prompts and inspire others with the power of your imagination"
      data={posts}
      handleEdit={handleEdit}
      handleDelete={handleDelete}
    />
  );
}

export default MyProfile