'use client'

import { useDebounce } from '@utils/useDebounce'
import React, { useState, useEffect } from 'react'
import PromptCard from './PromptCard'

const PromptCardList = ({data, handleTagClick}) => {
  return (
    <div className='mt-16 prompt_layout'>
      {data.map(post => (
        <PromptCard
          key={post._id}
          handleTagClick={() => handleTagClick(post.tag)}
          post={post}
        />
      ))}
    </div>
  )
}

const Feed = () => {

  const [searchText, setSearchText] = useState('');
  const [posts, setPosts] = useState([]);
  const debouncedSearchText = useDebounce(searchText, 1000)

  useEffect(() => {
    const fetchPosts = async () => {
      const response = await fetch('/api/prompt');

      const data = await response.json();

      setPosts(data)
    }
  
    fetchPosts();
  }, [])

  useEffect(() => {
    if(debouncedSearchText) {
      const fetchSearchPosts = async () => {
        const response = await fetch('/api/prompt/search?' + new URLSearchParams({
          q: debouncedSearchText
        }));

        const data = await response.json();

        setPosts(data)
      };

      fetchSearchPosts();
    }
  }, [debouncedSearchText]);

  const handleTagClick = (tag) => {
    setSearchText(tag)
  }
  

  const handleSearchChange = (e) => {
    e.preventDefault();
    setSearchText(e.target.value)
  }

  console.log(posts)

  return (
    <section className='feed'>
      <form className='relative w-full flex-center'>
        <input 
          type="text" 
          className='search_input peer'
          placeholder='Search for a tag or an username'
          value={searchText} 
          onChange={handleSearchChange} 
          required 
        />
      </form>

      <PromptCardList data={posts} handleTagClick={handleTagClick}/>
    </section>
  ) 
}

export default Feed