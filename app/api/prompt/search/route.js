import Prompt from "@models/prompt";

export const GET = async (request) => {
  const search = request.url.split('?q=')[1];

  try {
    const response = await Prompt.find({
      $or: [
        {
          prompt: { $regex: search, $options: 'i' },
        },
        {
          tag: { $regex: search, $options: 'i' },
        },
      ],
    }).populate('creator');

    console.log('RESPONSE', response)
    
    return new Response(JSON.stringify(response), {
      status: 200
    })

  } catch (error) {
    return new Response('Failed to get prompt', {
      status: 500
    })
  }
}