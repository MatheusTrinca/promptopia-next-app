import Prompt from "@models/prompt";
import { connectDB } from "@utils/database"

export const GET = async (request, { params }) => {
  try {
    await connectDB();

    const prompts = await Prompt.findById(params.id).populate('creator');

    return new Response(JSON.stringify(prompts), {
      status: 200
    })
  } catch (error) {
    return new Response('Failed to get prompts', {
      status: 500
    })
  }
}

export const PATCH = async(request, { params }) => {
  const { prompt, tag } = await request.json();

  try {
    await connectDB();

    const existingPrompt = await Prompt.findById(params.id);

    if(!existingPrompt) {
      return new Response(`Prompt not found with id ${params.id}`, {
        status: 404
      })
    }

    existingPrompt.prompt = prompt;
    existingPrompt.tag = tag;

    await existingPrompt.save();

    return new Response(JSON.stringify(existingPrompt), {
      status: 200
    })
    
  } catch (error) {
    return new Response('Failed to update prompt', {
      status: 500
    })
  }
}

export const DELETE = async(request, { params }) => {
  try {
    await connectDB();

    await Prompt.findByIdAndDelete(params.id);

    return new Response('Prompt deleted successfully', {
      status: 200
    })
    
  } catch (error) {
    return new Response('Failed to delete prompt', {
      status: 500,
    });
  }
}